import { Injectable } from '@angular/core';
import { LoginI } from '../Pattern/ILogin';
import { ResponseI } from '../Pattern/IResponseLogin';
import { IResponseList } from '../Pattern/IResponseList';
import { ISubscriber } from '../Pattern/ISubscriber';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  Url: string = 'https://lab.arkbox.co/api/';
  Token = localStorage.getItem('Token');
  Headers =  new HttpHeaders({      
    'Authorization' : `Bearer ${this.Token}`
  });
  Status!:string;

  constructor(private Http:HttpClient) { }

    LoginUser(form:LoginI):Observable<ResponseI>{
      let Address = this.Url + 'account/login';
      const Response = this.Http.post<ResponseI>(Address, form);    
      return Response;
    }

    GetAllSubscribers(): Observable<IResponseList>{ 
      return this.Http.get<IResponseList>(this.Url + `subscribers/`,{
      headers: this.Headers
      })
    }

    GetSubscriber(Id:number):Observable<ISubscriber>{
      const Address = this.Url + `subscribers/${Id}`;
      return this.Http.get<ISubscriber>(Address, {
        headers: this.Headers
      });
    }

    UpdateSubscriber(subscriber:ISubscriber):Observable<number>{
      const Address = this.Url + `subscribers/${subscriber.Id}`;
      return this.Http.put<number>(Address, {
        headers: this.Headers,
        body: subscriber
      });
    }

    CreateSubscriber(subscriber:ISubscriber):Observable<ISubscriber>{
      const Address = this.Url + `subscribers/`;
      return this.Http.post<ISubscriber>(Address, {
        headers: this.Headers,
        body: subscriber
      });
    }

    DeleteSubscriber(Id:number): Observable<{}>{
      const Address = this.Url + `subscribers/${Id}`;
      return this.Http.delete(Address, {
        headers: this.Headers
      });
    }

}
