import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'Pagination'
})
export class PaginationPipe implements PipeTransform {

  transform(array:any[], Page_size: number | string, Page_number: number): any[]{
    if(!array.length)return []
    if(Page_size === 'all') {
      return array
    }

    Page_size = Page_size || 3
    Page_number = Page_number || 1
    --Page_number
    // @ts-ignore
    return array.slice(Page_number * Page_size, (Page_number + 1) * Page_size)
  }
}
