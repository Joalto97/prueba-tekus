import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../Service/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';

import { IResponseList } from '../../Pattern/IResponseList';
import { ISubscriber } from '../../Pattern/ISubscriber';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-list-subcribers',
  templateUrl: './list-subcribers.component.html',
  styleUrls: ['./list-subcribers.component.css']
})
export class ListSubcribersComponent implements OnInit {

  Clients!:IResponseList;
  Criteria = '';
  Count = 10;
  Page = 1;
  SortOrder = 'PublicId';
  SortType = 0;

  constructor(private Api:ApiService, 
    private Router: Router,
    private ActivatedRoute:ActivatedRoute,
    private FormBuilder:FormBuilder) { }

  ngOnInit(): void {
    this.Api.GetAllSubscribers().subscribe(resp =>{
      this.Clients = resp;
    })
  }

  GetSubscriber(Id:number){
    this.Router.navigate(['Detail', Id]);
  } 

  CreateSubscriber(){
    this.Router.navigate(['New']);
  }

  handlePage(e: PageEvent){
    this.Page_size = e.pageSize
    this.Page_number = e.pageIndex + 1
  }

  Page_size:number = 3
  Page_number:number = 1
  PageSizeOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
}
