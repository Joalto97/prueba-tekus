import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { ApiService } from '../../Service/api.service';
import { ISubscriber } from '../../Pattern/ISubscriber';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {

  constructor(private Router:Router,
  private ActivateRoute:ActivatedRoute,
  private ApiService:ApiService) { }

  Subscriber!:ISubscriber;

  CreateSubscriberForm = new FormGroup({
    Name : new FormControl('', Validators.required),
    Email : new FormControl('', Validators.required),
    PhoneNumber : new FormControl('', Validators.required),
    CountryName : new FormControl(''),
    CountryCode : new FormControl(''),
    JobTitle : new FormControl(''),
    Area : new FormControl('')
  })

  ngOnInit(): void {
    let CreateSubscriber = Number(this.ActivateRoute.snapshot.paramMap.get('Id'));
    this.ApiService.GetSubscriber(CreateSubscriber).subscribe((resp) => {
      this.Subscriber = resp;  
      console.log(resp);   
    });
  }

  OnCreate(Form:ISubscriber): void{
    this.Subscriber.Name = this.Subscriber.Name;
    this.Subscriber.Email = this.Subscriber.Email;
    this.Subscriber.PhoneNumber = this.Subscriber.PhoneNumber;
    this.Subscriber.CountryName = this.Subscriber.CountryName;
    this.Subscriber.CountryCode = this.Subscriber.CountryCode;
    this.Subscriber.JobTitle = this.Subscriber.JobTitle;
    this.Subscriber.Area = this.Subscriber.Area;

    this.ApiService.CreateSubscriber(this.Subscriber).subscribe((resp) =>{
      this.Router.navigate(['Detail', resp]);
    });
  }

}
