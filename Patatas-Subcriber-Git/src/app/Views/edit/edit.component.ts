import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { ApiService } from '../../Service/api.service';
import { ISubscriber } from '../../Pattern/ISubscriber';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  constructor( private Router: Router,
    private Activateroute: ActivatedRoute,
    private ApiService: ApiService) { }

    Subscriber!: ISubscriber; 

    UpdateSubscriberForm = new FormGroup({
      Name : new FormControl('', Validators.required),
      Email : new FormControl('', Validators.required),
      PhoneNumber : new FormControl(''),
      CountryName : new FormControl(''),
      CountryCode : new FormControl(''),
      JobTitle : new FormControl(''),
      Area : new FormControl('')
    })

  ngOnInit(): void {
    let EditSubscriber = Number(this.Activateroute.snapshot.paramMap.get('Id'));
    this.ApiService.GetSubscriber(EditSubscriber).subscribe((resp) => {
      this.Subscriber = resp;  
      console.log(resp);   
    });
  }

  OnUpdate(form: ISubscriber): void {
    this.Subscriber.Name = form.Name?? this.Subscriber.Name;
    this.Subscriber.Email = form.Name?? this.Subscriber.Email;
    this.Subscriber.PhoneNumber = form.Name?? this.Subscriber.PhoneNumber;
    this.Subscriber.CountryName = form.Name?? this.Subscriber.CountryName;
    this.Subscriber.CountryCode = form.Name?? this.Subscriber.CountryCode;
    this.Subscriber.JobTitle = form.Name?? this.Subscriber.JobTitle;
    this.Subscriber.Area = form.Name?? this.Subscriber.Area;
    
    this.ApiService.UpdateSubscriber(this.Subscriber).subscribe((resp) => {
      this.Router.navigate(['Detail', resp]);
    });
  }

}
