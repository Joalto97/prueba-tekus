import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../Service/api.service';
import { ISubscriber } from '../../Pattern/ISubscriber';

@Component({
  selector: 'app-subscriber-detail',
  templateUrl: './subscriber-detail.component.html',
  styleUrls: ['./subscriber-detail.component.css']
})
export class SubscriberDetailComponent implements OnInit {

  constructor(private Router:Router, 
    private ActivateRoute:ActivatedRoute,
    private ApiService:ApiService) { }
    public Subscriber!: ISubscriber;
    public Keys!: any;
    public Response = {};

  ngOnInit(): void {
    let EditSubscriber = Number(this.ActivateRoute.snapshot.paramMap.get('Id'));    
    this.ApiService.GetSubscriber(EditSubscriber).subscribe(resp => {
      this.Subscriber = resp;
      this.Keys = Object.entries(resp);
    });
  }

  OnDeleteSubscriber(Id:number){
    this.ApiService.DeleteSubscriber(Id).subscribe(resp => {
      this.Response = resp;
      this.Router.navigate(['Subscribers']);
    });
  }

  OnEditSubscriber(Id:number){
    this.Router.navigate(['Edit', Id]);
  }

}
