import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule, ComponentPath } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeadersComponent } from './Templates/headers/headers.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PaginationPipe } from './Pipes/pagination.pipe';
import { MatPaginatorModule, MatPaginatorIntl } from '@angular/material/paginator';
import { CustomMatPaginatorIntl } from './Views/list-subcribers/Paginator.En';

@NgModule({
  declarations: [
    AppComponent,
    HeadersComponent,
    ComponentPath,
    PaginationPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatPaginatorModule
  ],
  providers: [
    {
      provide:MatPaginatorIntl,
      useClass:CustomMatPaginatorIntl
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
