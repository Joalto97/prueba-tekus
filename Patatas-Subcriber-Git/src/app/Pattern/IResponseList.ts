import {ISubscriber} from './ISubscriber';

export interface IResponseList{
   Count: number; 
   Data: ISubscriber[]
}